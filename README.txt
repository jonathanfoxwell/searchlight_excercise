
Creator - Jonathan Foxwell


Video walkthrough
https://www.loom.com/share/02717f1fcf444492a7536c312a51aba9


Setup Instructions
[api] rename .env.example to .env
[api] .env - set local MySQL DB credentials
[api] composer install
[api] php artisan migrate
[api] php artisan db:seed
[api] php artisan test
[api] php artisan serve --host=localhost --port=8000

[client] npm install
[client] npm run serve

