import { createStore } from 'vuex'
import axios from 'axios'
import auth from './auth'
import Vue from 'vue'

export default createStore({
  state: {
    userMovieView: false,
    allMovies:[],
    userMovies:[]
  },
  mutations: {

    SET_USER_MOVIE_VIEW(state, boolean){
      state.userMovieView = boolean;
    },

    SET_ALL_MOVIES(state, allMovies){
      state.allMovies = allMovies
    },

    SET_USER_MOVIES(state, userMovies){
      state.userMovies = userMovies
    },

    
  },

  actions: {

    /*
    *  payload expected - movie title
    */
    async create_movie ({ commit, getters }, payload){
      await axios.post('/api/movie', payload)
      .then((response) => {
        console.log(response.data);

      }, (error) => {
        console.log(error);
      }); 
    },

    async get_all_movies ({ commit, getters }){
        await axios.get('/api/all_movies')
        .then((response) => {
          commit('SET_ALL_MOVIES', response.data.all_movies);
          console.log('state - allmovies - '+ JSON.stringify(this.state.allMovies));
          

        }, (error) => {
          console.log(error);
        }); 
    },

    /*
    *  payload expected - user_id
    */
    async get_user_movies ({ commit, getters }){
      let user_id = getters['auth/user']['id'];

      await axios.get('/api/user_movies/'+user_id)
      .then((response) => {
        commit('SET_USER_MOVIES', response.data.user_movies);
        // console.log(response.data.user_movies);
        

      }, (error) => {
        console.log(error);
      }); 
    },

    /*
    *  payload expected - user_id, movie_id
    */
    async add_user_movie ({ commit, getters }, payload){
      let user_id = getters['auth/user']['id'];
      let details = {
        'user_id': user_id,
        'movie_id': payload
      }
      await axios.post('/api/user_movie', details)
      .then((response) => {
        console.log(response.data);

      }, (error) => {
        console.log(error);
      }); 
    },

    /*
    *  payload expected - user_id, movie_id
    */
    async remove_user_movie ({ commit, getters }, payload){
      let user_id = getters['auth/user']['id'];
      let details = {
        'user_id': user_id,
        'movie_id': payload
      }
      await axios.delete('/api/user_movie', { params: details } )
      .then((response) => {
        console.log(response.data);

      }, (error) => {
        console.log(error);
      }); 
    },
  },
  
  modules: {
    auth
  }
})
