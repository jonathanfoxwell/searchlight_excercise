<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MovieUser extends Model
{
    public $table = 'movie_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'movie_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];
}
