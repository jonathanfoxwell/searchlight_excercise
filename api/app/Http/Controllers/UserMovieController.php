<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Models\MovieUser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class UserMovieController extends Controller
{
    public function get_user_movies(Request $request, $user_id)
    {
        $params = ['user_id' => $user_id];
        $validator = Validator::make($params, [
            'user_id' => 'required|exists:users,id'
        ]);
        if ($validator->fails()) {
            $validRequest = false;
        } else {
            $validRequest = true;

            //process
            $user = $user_id;
            $userMovies = Movie::whereHas('users', function ($query) use ($user) {
                return $query->where('user_id', '=', $user);
            })->get();
        }

        //respond
        // return JSON response to Axios
        if ($validRequest) {
            return response()->json([
                'user_movies' => $userMovies
            ], Response::HTTP_OK);
        } else {
            return response()->json([
                'status' => 'Invalid request for user movies'
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function add_user_movie(Request $request)
    {
        //validate
        // TODO - validate unique combination to avoid duplication
        $validator = Validator::make($request->all(), [
            'movie_id' => 'required|exists:movies,id',
            'user_id' => 'required|exists:users,id',
        ]);
        if ($validator->fails()) {
            $validRequest = false;
        } else {
            $validRequest = true;

            // process
            $movieUser = new MovieUser();
            $movieUser->user_id = $request->user_id;
            $movieUser->movie_id = $request->movie_id;
            $saved = $movieUser->save();
        }

        // respond
        if (!$validRequest) {
            $APIresponse = 'Your request was invalid, and could not be processed';
            return response()->json([
                'status' => $APIresponse
            ], Response::HTTP_BAD_REQUEST);
        } elseif (!$saved) {
            $APIresponse = 'Could not save your movie';
            return response()->json([
                'status' => $APIresponse
            ], Response::HTTP_BAD_REQUEST);
        } else {
            $APIresponse = 'Movie created';
            return response()->json([
                'status' => $APIresponse
            ], Response::HTTP_OK);
        }
    }

    public function remove_user_movie(Request $request)
    {
        //validate
        $validator = Validator::make($request->all(), [
            'movie_id' => 'required|exists:movies,id',
            'user_id' => 'required|exists:users,id'
        ]);
        if ($validator->fails()) {
            $validRequest = false;
        } else {
            $validRequest = true;

            // process
            $movieUser = MovieUser::where('user_id', $request->user_id)
                            ->where('movie_id', $request->movie_id)
                            ->first();
            if ($movieUser === null) {
                $removed = false;
            } else {
                $movieUser->delete();
                $removed = true;
            }
        }

        // respond
        if (!$validRequest) {
            $APIresponse = 'Your request was invalid, and could not be processed';
            return response()->json([
                'status' => $APIresponse
            ], Response::HTTP_BAD_REQUEST);
        } elseif (!$removed) {
            $APIresponse = 'Could not remove your movie';
            return response()->json([
                'status' => $APIresponse
            ], Response::HTTP_BAD_REQUEST);
        } else {
            $APIresponse = 'Movie removed';
            return response()->json([
                'status' => $APIresponse
            ], Response::HTTP_OK);
        }
    }
}
