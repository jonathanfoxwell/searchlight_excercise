<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class MovieController extends Controller
{
    public function get_all_movies()
    {
        // return JSON response to Axios
        return response()->json([
            'all_movies' => Movie::all()
        ], Response::HTTP_OK);
    }

    public function create_movie(Request $request)
    {
        // validate
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:movies,title|max:255',
        ]);
        if ($validator->fails()) {
            $validRequest = false;
        } else {
            $validRequest = true;

            // process
            $movie = new Movie;
            $movie->title = $request->title;
            $saved = $movie->save();
        }

        // respond
        if (!$validRequest) {
            $APIresponse = 'Your movie title is invalid';
            return response()->json([
                'status' => $APIresponse
            ], Response::HTTP_BAD_REQUEST);
        } elseif (!$saved) {
            $APIresponse = 'Could not save your movie';
            return response()->json([
                'status' => $APIresponse
            ], Response::HTTP_BAD_REQUEST);
        } else {
            $APIresponse = 'Movie created';
            return response()->json([
                'status' => $APIresponse
            ], Response::HTTP_OK);
        }
    }
}
