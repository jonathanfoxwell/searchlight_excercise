<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $firstNames = ['Sally', 'Billy', 'James', 'Annette', 'Anne', 'Sam', 'Robin', 'Grant'];

        foreach ($firstNames as $firstName) {
            $firstNameLower = strtolower($firstName);
            $user = new User();
            $user->password = Hash::make($firstNameLower);
            $user->email = $firstNameLower . '@example.com';
            $user->name = $firstName . ' Example';
            $user->save();
        }
    }
}
