<?php

namespace Database\Seeders;

use App\Models\MovieUser;
use Illuminate\Database\Seeder;

class MovieUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $movieUser = new MovieUser();
        $movieUser->user_id = 1;
        $movieUser->movie_id = 1;
        $movieUser->save();
    }
}
