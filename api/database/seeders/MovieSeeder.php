<?php

namespace Database\Seeders;

use App\Models\Movie;
use Illuminate\Database\Seeder;

class MovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Movies = [
            'Once Upon a Time in Hollywood',
            'The Good the Bad and the Ugly',
            'Casino Royal',
            'The Matrix',
            'Gladiator',
            'Terminator 2',
            'Forrest Gump',
            'A Fistful of Dollars'
        ];

        foreach ($Movies as $Movie) {
            $movie = new Movie();
            $movie->title = $Movie ;
            $movie->save();
        }
    }
}
