<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\UserMovieController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', LoginController::class);
Route::middleware('auth:sanctum')->post('/movie', [MovieController::class, 'create_movie']);
Route::middleware('auth:sanctum')->get('/all_movies', [MovieController::class, 'get_all_movies']);
Route::middleware('auth:sanctum')->get('/user_movies/{params}', [UserMovieController::class, 'get_user_movies']);
Route::middleware('auth:sanctum')->post('/user_movie', [UserMovieController::class, 'add_user_movie']);
Route::middleware('auth:sanctum')->delete('/user_movie', [UserMovieController::class, 'remove_user_movie']);
