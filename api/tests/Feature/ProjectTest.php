<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ProjectTest extends TestCase
{
    use WithFaker;

    /**
     * @test
     */
    public function seeded_user_exists()
    {
        // $this->withoutExceptionHandling();

        $this->assertDatabaseHas('users', [
            'email' => 'sally@example.com',
        ]);
    }

    /**
     * @test
     */
    public function seeded_movie_exists()
    {
        $this->assertDatabaseHas('movies', [
            'title' => 'Once Upon a Time in Hollywood',
        ]);
    }

    /**
     * @test
     */
    public function user_can_create_movie()
    {
        $title = $this->faker->sentence;

        $user = Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );

        $payload = ['title' => $title];

        $response = $this->post('/api/movie', $payload);

        $this->assertDatabaseHas('movies', [
            'title' => $title,
        ]);
    }

    /**
     * @test
     */
    public function user_can_view_all_movies_list()
    {
        $user = Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );

        $seededMovie = 'Once Upon a Time in Hollywood';

        $response = $this->get('/api/all_movies');

        $response->assertSee($seededMovie, $escaped = true);
    }

    /**
     * @test
     */
    public function user_can_view_their_movies_list()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );

        // $payload = ['user_id' => 1];
        // $user_id = 1;
        $string = '1';

        $response = $this->get('/api/user_movies/' . $string);

        $seededMovie1User1 = 'Once Upon a Time in Hollywood';

        $response->assertSee($seededMovie1User1, $escaped = true);
    }

    /**
     * @test
     */
    public function user_can_add_movie_to_their_list()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );

        $payload = ['user_id' => 1, 'movie_id' => 2];

        $response = $this->post('/api/user_movie', $payload);

        $response->assertSee('Movie created', $escaped = true);
    }

    /**
     * @test
     */
    public function user_can_remove_movie_from_their_list()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );

        $payload = ['user_id' => 1, 'movie_id' => 2];

        $response = $this->delete('/api/user_movie', $payload);

        $response->assertSee('Movie removed', $escaped = true);
    }
}
